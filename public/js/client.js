'use strict';

const mediaStreamConstraints = {
    video: true,
};

const mediaRecorderConstraints = {
    mimeType: 'video/webm\;codecs=vp9'
};

const socket = io({
    transports: ['websocket'],
    secure: true,
    autoConnect: false,
});

// Elements
const remoteVideo = document.querySelector('#remoteVideo');
const localVideo = document.querySelector('#localVideo');
const startButton = document.querySelector('#startButton');
const callButton = document.querySelector('#callButton');
const stopButton = document.querySelector('#hangupButton');

let localStream;
let mediaRecorder;
let sourceBuffer;
let fileReader = new FileReader();

function handleLocalMediaStreamError(error) {
    console.error('navigator.getUserMedia error: ', error);
}

function gotLocalMediaStream(mediaStream) {
    localStream = mediaStream;
    localVideo.srcObject = mediaStream;

    mediaRecorder = new MediaRecorder(mediaStream, mediaRecorderConstraints);
    mediaRecorder.addEventListener('dataavailable', handleDataAvailable);

    startButton.disabled = true;
    callButton.disabled = false;
    stopButton.disabled = false;
}

function startRecording() {
    if (!socket.disconnected) {
        mediaRecorder.start(100);
        console.log('recording...');
    } else {
        console.warn('socket disconnected');
    }
}

function stopRecording() {
    if (socket.disconnected) {
        localStream.getVideoTracks().forEach(track => track.stop());
        localStream.getAudioTracks().forEach(track => track.stop());
        sourceBuffer.abort();
        mediaRecorder.stop();
        console.log('recording stopped');
        return true;
    } else {
        console.warn('socket still connected');
    }

    return false;
}

function call(e) {
    try {
        console.log('connecting...');
        socket.connect();
    } catch (e) {
        console.error(`call error: ${e.name}`, e);
    }
}

function stop(e) {
    try {
        console.log('disconnecting...');
        socket.disconnect();
    } catch(e) {
        console.error(`hang out error: ${e.name}`, e);
    }
}

async function start(e) {
    try {
        navigator.mediaDevices.getUserMedia(mediaStreamConstraints)
            .then(gotLocalMediaStream)
            .catch(handleLocalMediaStreamError);
    } catch (e) {
        console.error(`getUserMedia error: ${e.name}`, e);
    }
}

function createMediaSource() {
    try {
        const mediaSource = new MediaSource();
        remoteVideo.src = URL.createObjectURL(mediaSource);
        mediaSource.addEventListener('sourceopen', e => onSourceOpen(mediaSource));
        mediaSource.addEventListener('sourceended', e => createMediaSource());
    } catch(e) {
        console.error(`creating media source: ${e.name}`, e);
    }
}

function onSourceOpen(mediaSource) {
    try {
        sourceBuffer = mediaSource.addSourceBuffer(mediaRecorderConstraints.mimeType);
    } catch (e) {
        console.error(`onSourceOpen error: ${e.name}`, e);
    }
}

function handleDataAvailable(event) {
    const blob = event.data;
    if (blob && blob.size > 0) {
        if (fileReader.readyState !== FileReader.LOADING) {
            fileReader.readAsArrayBuffer(blob);
        }
        // const response = new Response(blob);
        // response.arrayBuffer()
        //     .then((data) => {
        //         sourceBuffer.appendBuffer(new Uint8Array(data));
        //     });
    }
}

socket.on('reconnect_attempt', () => {
    socket.io.opts.transports = ['polling', 'websocket'];
});

socket.on('connect', () => {
    console.log('socket connected.');
    callButton.disabled = true;
    startRecording();
});

socket.on('disconnect', (reason) => {
    console.log(`socket disconnected: ${reason}`);
    if (stopRecording()) {
        startButton.disabled = false;
        callButton.disabled = true;
        stopButton.disabled = true
    }
});

socket.on('video', (data) => {
    if (sourceBuffer) {
        sourceBuffer.appendBuffer(new Uint8Array(data));
    }
});

fileReader.onload = function(event) {
    let arrayBuffer = event.target.result;
    socket.binary(true).emit('video', arrayBuffer);
};

createMediaSource();
startButton.addEventListener('click', e => start());
callButton.addEventListener('click', e => call());
stopButton.addEventListener('click', e => stop());