/**
 * Created by oscar on 7/27/2019.
 */
$(function () {
    const socket = io();
    $('form').submit(function(e){
        e.preventDefault(); // prevents page reloading
        socket.emit('message', $('#m').val());
        $('#m').val('');
        return false;
    });
    socket.on('message', function(msg){
        $('#messages').append($('<li>').text(msg));
    });
});