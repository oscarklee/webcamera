/**
 * Created by oscar on 7/27/2019.
 */
const express = require('express');
const app = express();
const fs = require('fs');
const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
};
const https = require('https').createServer(options, app);
const io = require('socket.io')(https);
const path = require('path');

const dirName = __dirname + '/public';
const port = 443;

app.use('/', express.static(path.join(dirName)));

app.get('/', function (req, res) {
    res.sendFile(dirName + '/index.html');
});

io.on('connection', function(socket){
    socket.on('video', function(data){
        socket
            // .broadcast
            .volatile
            .binary(true)
            .emit('video', data);
    });
});

https.listen(port, function() {
    console.log(`listening on *: ${port}`);
});